//
// Created by vad1mchk on 11/16/22.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_BMP_HEADER_H
#define ASSIGNMENT_3_IMAGE_ROTATION_BMP_HEADER_H

#include "image.h"

#define DEFAULT_FTYPE_1 0x424D
#define DEFAULT_FTYPE_2 0x4D42
#define DEFAULT_RESERVED 0
#define DEFAULT_SIZE 40
#define DEFAULT_BIT_COUNT 24
#define DEFAULT_PLANES 1
#define DEFAULT_COMPRESSION 0
#define DEFAULT_PELS_PER_METER 2834
#define DEFAULT_CLR_USED 0
#define DEFAULT_CLR_IMPORTANT 0

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

extern struct bmp_header default_header;

void print_header(const struct bmp_header header);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_BMP_HEADER_H
