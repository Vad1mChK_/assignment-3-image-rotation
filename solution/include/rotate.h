//
// Created by vad1mchk on 11/16/22.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_ROTATE_H

#include "image.h"

struct image rotate(const struct image source);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_ROTATE_H
