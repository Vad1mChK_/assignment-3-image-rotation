//
// Created by vad1mchk on 11/16/22.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_FILE_OPEN_CLOSE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_FILE_OPEN_CLOSE_H
#include <stdio.h>

enum open_status {
    OPEN_OK = 0,
    OPEN_WRONG_ARGUMENTS_COUNT,
    OPEN_INPUT_FILE_NOT_FOUND,
    OPEN_INPUT_FILE_INSUFFICIENT_PRIVILEGES,
    OPEN_OUTPUT_FILE_INSUFFICIENT_PRIVILEGES,
    OPEN_INPUT_FILE_WTF,
    OPEN_OUTPUT_FILE_WTF
};

enum close_status {
    CLOSE_OK = 0,
    CLOSE_INPUT_FILE_BAD_DESCRIPTOR,
    CLOSE_OUTPUT_FILE_BAD_DESCRIPTOR,
    CLOSE_INPUT_FILE_WTF,
    CLOSE_OUTPUT_FILE_WTF
};

extern const char *open_error_messages[];

extern const char *close_error_messages[];

enum open_status open_input_file(const char *filename, FILE **input_file);

enum open_status open_output_file(const char *filename, FILE **output_file);

enum close_status close_input_file(FILE *input_file);

enum close_status close_output_file(FILE *output_file);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_FILE_OPEN_CLOSE_H
