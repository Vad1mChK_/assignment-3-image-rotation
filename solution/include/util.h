//
// Created by vad1mchk on 11/16/22.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_UTIL_H
#define ASSIGNMENT_3_IMAGE_ROTATION_UTIL_H

#include <stdbool.h>

void println(const char *message);

void println_err(const char *message);

void print_newline(void);

bool yes_no(bool default_value);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_UTIL_H
