//
// Created by vad1mchk on 11/16/22.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_FILE_READ_WRITE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_FILE_READ_WRITE_H

#include "image.h"
#include <stdio.h>

enum read_status {
    READ_OK = 0,
    READ_INVALID_HEADER,
    READ_INVALID_BITS
};

/*  serializer   */
enum write_status {
    WRITE_OK = 0,
    WRITE_HEADER_ERROR,
    WRITE_DATA_ERROR
};

extern const char *read_error_messages[];

extern const char *write_error_messages[];

enum read_status from_bmp(FILE *in, struct image *img);

enum write_status to_bmp(FILE *out, struct image const *img);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_FILE_READ_WRITE_H
