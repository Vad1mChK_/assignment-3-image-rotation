//
// Created by vad1mchk on 11/17/22.
//

#include "../include/bmp_header.h"
#include <stdio.h>

struct bmp_header default_header = (struct bmp_header) {
        .bfType = DEFAULT_FTYPE_2,
        // .bfileSize;
        .bfReserved = DEFAULT_RESERVED,
        .bOffBits = DEFAULT_SIZE + 14,
        .biSize = DEFAULT_SIZE,
        // .biWidth;
        // .biHeight;
        .biPlanes = DEFAULT_PLANES,
        .biBitCount = DEFAULT_BIT_COUNT,
        .biCompression = DEFAULT_COMPRESSION,
        // .biSizeImage;
        .biXPelsPerMeter = DEFAULT_PELS_PER_METER,
        .biYPelsPerMeter = DEFAULT_PELS_PER_METER,
        .biClrUsed = DEFAULT_CLR_USED,
        .biClrImportant = DEFAULT_CLR_IMPORTANT
};

void print_header(struct bmp_header header) {
    printf(
            "bmp {\n"
            "\tbfType = %" PRId16 "\n"
            "\tbfileSize = %" PRId32 "\n"
            "\tbfReserved = %" PRId32 "\n"
            "\tbOffBits = %" PRId32 "\n"
            "\tbiSize = %" PRId32 "\n"
            "\tbiWidth = %" PRId32 "\n"
            "\tbiHeight = %" PRId32 "\n"
            "\tbiPlanes = %" PRId16 "\n"
            "\tbiBitCount = %" PRId16 "\n"
            "\tbiCompression = %" PRId32 "\n"
            "\tbiSizeImage= %" PRId32 "\n"
            "\tbiXPelsPerMeter = %" PRId32 "\n"
            "\tbiYPelsPerMeter = %" PRId32 "\n"
            "\tbiClrUsed = %" PRId32 "\n"
            "\tbiClrImportant = %" PRId32 "\n"
            "}\n",
            header.bfType, header.bfileSize, header.bfReserved, header.bOffBits,
            header.biSize, header.biWidth, header.biHeight, header.biPlanes,
            header.biBitCount, header.biCompression, header.biSizeImage,
            header.biXPelsPerMeter, header.biYPelsPerMeter, header.biClrUsed, header.biClrImportant
    );
}
