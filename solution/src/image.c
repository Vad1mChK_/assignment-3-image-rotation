//
// Created by vad1mchk on 11/17/22.
//
#include "../include/image.h"
#include <malloc.h>

void destroy_image(struct image *img) {
    free(img->data);
    img->width = 0;
    img->height = 0;
}
