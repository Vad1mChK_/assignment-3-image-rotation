//
// Created by vad1mchk on 11/16/22.
//

#include "../include/util.h"
#include <stdio.h>

void println(const char *message) {
    printf("%s\n", message);
}

void println_err(const char *message) {
    fprintf(stderr, "\033[1mimage-transformer:\033[31m error\033[0m: %s\n", message);
}

void print_newline(void) {
    printf("\n");
}

bool yes_no(bool default_value) {
    char prompt;
    printf(default_value ? "(Y/n): " : "(y/N): ");
    prompt = (char) getchar();
    if (default_value) {
        return prompt == 'N' || prompt == 'n';
    } else {
        return prompt == 'Y' || prompt == 'y';
    }
}
