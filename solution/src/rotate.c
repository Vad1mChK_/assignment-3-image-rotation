//
// Created by vad1mchk on 11/16/22.
//
#include "../include/rotate.h"
#include <malloc.h>

struct image rotate(const struct image source) {
    uint32_t width = source.width;
    uint32_t height = source.height;
    struct pixel *old_pixels = source.data;
    struct pixel *new_pixels = malloc(sizeof(struct pixel) * width * height);
    for (uint32_t i = 0; i < height; i++) {
        for (uint32_t j = 0; j < width; j++) {
            new_pixels[j * height + (height - 1 - i)] = old_pixels[i * width + j];
        }
    }
    return (struct image) {.width = height, .height = width, .data = new_pixels};
}
