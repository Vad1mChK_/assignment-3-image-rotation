//
// Created by vad1mchk on 11/16/22.
//

#include "../include/util.h"
#include "../include/file_open_close.h"
#include <errno.h>
#include <stdio.h>
#include <unistd.h>

enum open_status open_input_file(const char *filename, FILE **input_file) {
    if (access(filename, F_OK)) {
        return OPEN_INPUT_FILE_NOT_FOUND;
    }
    if (access(filename, R_OK)) {
        return OPEN_INPUT_FILE_INSUFFICIENT_PRIVILEGES;
    }
    *input_file = fopen(filename, "rb");
    if (*input_file == NULL) {
        return OPEN_INPUT_FILE_WTF;
    }
    return OPEN_OK;
}

enum open_status open_output_file(const char *filename, FILE **output_file) {
    *output_file = fopen(filename, "wb");
    if (*output_file == NULL) {
        return OPEN_OUTPUT_FILE_WTF;
    }
    return OPEN_OK;
}

enum close_status close_input_file(FILE *input_file) {
    if (fclose(input_file)) {
        if (errno == EBADF) {
            return CLOSE_INPUT_FILE_BAD_DESCRIPTOR;
        }
        return CLOSE_INPUT_FILE_WTF;
    }
    return CLOSE_OK;
}


enum close_status close_output_file(FILE *output_file) {
    if (fclose(output_file)) {
        if (errno == EBADF) {
            return CLOSE_OUTPUT_FILE_BAD_DESCRIPTOR;
        }
        return CLOSE_OUTPUT_FILE_WTF;
    }
    return CLOSE_OK;
}

const char *open_error_messages[] = {
        [OPEN_WRONG_ARGUMENTS_COUNT] = "Wrong arguments count. Please specify 2 filenames and retry.",
        [OPEN_INPUT_FILE_NOT_FOUND] = "Input file not found.",
        [OPEN_INPUT_FILE_INSUFFICIENT_PRIVILEGES] = "Not enough privileges to read from input file.",
        [OPEN_OUTPUT_FILE_INSUFFICIENT_PRIVILEGES] = "Not enough privileges to write to output file.",
        [OPEN_INPUT_FILE_WTF] = "Unknown problem with input file.",
        [OPEN_OUTPUT_FILE_WTF] = "Unknown problem with output file.",
};

const char *close_error_messages[] = {
        [CLOSE_INPUT_FILE_BAD_DESCRIPTOR] = "Cannot close input file properly: bad descriptor.",
        [CLOSE_OUTPUT_FILE_BAD_DESCRIPTOR] = "Cannot close output file properly: bad descriptor.",
        [CLOSE_INPUT_FILE_WTF] = "Cannot close input file properly: unknown problem.",
        [CLOSE_OUTPUT_FILE_WTF] = "Cannot close output file properly: unknown problem.",
};
