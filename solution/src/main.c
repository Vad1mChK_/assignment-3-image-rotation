#include "../include/file_open_close.h"
#include "../include/file_read_write.h"
#include "../include/rotate.h"
#include "../include/util.h"
#include <stdio.h>

int main(int argc, char **argv) {
    FILE *input_file;
    FILE *output_file;
    enum open_status o_status;
    enum read_status r_status;
    enum write_status w_status;
    enum close_status c_status;

    if (argc != 3) {
        println_err(open_error_messages[OPEN_WRONG_ARGUMENTS_COUNT]);
        return OPEN_WRONG_ARGUMENTS_COUNT;
    }

    o_status = open_input_file(argv[1], &input_file);
    if (o_status) {
        println_err(open_error_messages[o_status]);
        return 1;
    }

    struct image img;
    r_status = from_bmp(input_file, &img);
    if (r_status) {
        println_err(read_error_messages[r_status]);
        return 2;
    }

    c_status = close_input_file(input_file);
    if (c_status) {
        println_err(close_error_messages[c_status]);
        return 3;
    }

    struct image result = rotate(img);
    destroy_image(&img);

    o_status = open_output_file(argv[2], &output_file);
    if (o_status) {
        println_err(open_error_messages[o_status]);
        return 4;
    }

    w_status = to_bmp(output_file, &result);
    if (w_status) {
        println_err(write_error_messages[w_status]);
        return 5;
    }

    c_status = close_output_file(output_file);
    if (c_status) {
        println_err(close_error_messages[c_status]);
        return 6;
    }
    destroy_image(&result);
    return 0;
}
