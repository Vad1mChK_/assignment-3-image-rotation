//
// Created by vad1mchk on 11/16/22.
//

#include "../include/bmp_header.h"
#include "../include/file_read_write.h"
#include "../include/util.h"
#include <malloc.h>

const char *read_error_messages[] = {
        [READ_INVALID_HEADER] = "Invalid header encountered while reading file.",
        [READ_INVALID_BITS] = "Invalid bits encountered while reading file."
};

const char *write_error_messages[] = {
        [WRITE_HEADER_ERROR] = "Error while writing header.",
        [WRITE_DATA_ERROR] = "Error while writing data."
};

static int validate_header(struct bmp_header header);

static uint32_t get_padding(const uint32_t width);

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header;
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }
    if (validate_header(header)) return READ_INVALID_HEADER;
    println("Header read successfully:");
    print_header(header);

    uint32_t width = header.biWidth;
    uint32_t height = header.biHeight;
    uint32_t padding = get_padding(width);
    struct pixel *pixels = malloc(sizeof(struct pixel) * width * height);

    for (uint32_t i = 0; i < height; i++) {
        size_t pixels_count = fread(pixels + width * i, sizeof(struct pixel), width, in);
        printf("line %" PRId32 ": %zu pixels read\n", i, pixels_count);
        if (pixels_count != width || fseek(in, padding, SEEK_CUR)) return READ_INVALID_BITS;
    }
    img->width = width;
    img->height = height;
    img->data = pixels;
    println("Data read successfully.");
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    struct bmp_header header = default_header;
    uint32_t width = img->width;
    uint32_t height = img->height;
    uint32_t padding = get_padding(width);

    header.biWidth = width;
    header.biHeight = height;
    header.biSizeImage = (width * sizeof(struct pixel) + get_padding(width)) * height;
    header.bfileSize = header.bOffBits + header.biSizeImage;
    println("Header generated:");
    print_header(header);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_HEADER_ERROR;
    }
    println("Header written successfully.");

    struct pixel *pixels = img->data;
    for (int32_t i = 0; i < height; i++) {
        size_t pixels_count = fwrite(pixels + width * i, sizeof(struct pixel), width, out);
        printf("line %" PRId32 ": %zu pixels written\n", i, pixels_count);
        if (pixels_count != width) return WRITE_DATA_ERROR;
        if (padding != 0) {
            if (fwrite(&pixels, sizeof(uint8_t), padding, out) != padding) return WRITE_DATA_ERROR;
        }
    }
    println("Data written successfully.");

    return WRITE_OK;
}

static int validate_header(struct bmp_header header) {
    if (header.bfType != DEFAULT_FTYPE_1 && header.bfType != DEFAULT_FTYPE_2) return 1;
    if (header.biWidth < 0 || header.biHeight < 0 || header.biSizeImage < 0 || header.bOffBits < 14) return 1;
    // some other validation rules probably
    return 0;
}

static uint32_t get_padding(const uint32_t width) {
    return width & 3 ? (4 - width * sizeof(struct pixel) % 4) : 0; // could as well optimize as 4 - width % 4;
}
